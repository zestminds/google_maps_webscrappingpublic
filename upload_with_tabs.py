#!/usr/local/bin/python3
import csv
import re
import os,threading,time
import pandas as pd
from tkinter import *
from tkinter import ttk
from bs4 import BeautifulSoup
from tkinter import messagebox
from selenium import webdriver
from datetime import date,datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from tkinter.filedialog import askopenfilename ,asksaveasfilename
  
class App(Tk):
	def __init__(self):
	   Tk.__init__(self)
	   self.geometry("600x600")
	   self.resizable(0, 0)
	   self.notebook = ttk.Notebook()
	   self.add_tab()
	   self.notebook.pack(side="top", fill="both", expand=True)
	   # self.notebook.grid(row=0)
  
	def add_tab(self):
		tab = UploadCsv(self.notebook,controller=self)
		tab2 = UploadExcel(self.notebook,controller=self) 
		self.notebook.add(tab,text="Upload CSV")
		self.notebook.add(tab2,text="Upload Excel")
		
	def close_window(self):
		self.quit()

	def exportCSV (self,export_file_path):
		df.to_csv (export_file_path, index = False, header=True)
		messagebox.showinfo("Information", "CSV File Export successfully.")
		self.close_window()
		

  
class UploadCsv(Frame):
	def __init__(self,name,controller):
		Frame.__init__(self)
		global v,csv_file_path
		self.controller = controller
		w1 = Label(self, text="Upload CSV File:")
		w1.pack()
		w1.place(x=67, y=220)

		self.v = StringVar()
		e1 = Entry(self,width="35", textvariable=self.v)
		e1.pack()
		e1.place(x = 167, y = 220)

		b1 = Button(self,text = "browse" ,command = lambda:self.import_csv_data())	
		b1.pack()
		b1.place(x = 407, y = 220)


		b2 = Button(self,text = "upload",command = lambda:self.start_thread())	
		b2.pack()
		b2.place(x = 217, y = 280)
		
		b3 = Button(self,text = "stop",command = lambda:self.close_thread())	
		b3.pack()
		b3.place(x = 277, y = 280)
		
	def import_csv_data(self):
		self.csv_file_path = askopenfilename(filetypes =[('CSV Files', '*.csv')])
		self.v.set(self.csv_file_path)
			
	def start_thread(self):
		self.thread_running = True
		thread = threading.Thread(target=self.upload_csv)
		thread.start()
	
	def upload_csv(self):
		if self.csv_file_path:
			global filename,date_time,driver,data
			#for get filename
			base=os.path.basename(self.csv_file_path)
			filename = os.path.splitext(base)[0]
			#for current Datetime
			now = datetime.now() 
			date_time = now.strftime("%m.%d.%Y_%H%M")
			driver = webdriver.Chrome("chromedriver")
			driver.implicitly_wait(8)
			wait = WebDriverWait(driver, 8)
			data = []
			with open(''+self.csv_file_path+'', mode='r') as csv_file:
				csv_reader = csv.reader(csv_file, delimiter=',')
				for row in csv_reader:
					try:
						new_row = ''.join([row[0], row[1],row[2]])
					except IndexError:
						new_row = ''.join(row)
					driver.get(new_row)
					element = wait.until_not(
					EC.presence_of_element_located((
					  By.XPATH, "/html/body/div/div/*[name()='svg']")))
					WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[@jsaction='pane.placeActions.share']"))).click()
					content = driver.page_source
					soup = BeautifulSoup(content,'html.parser')
					
					# currentMapUrl = WebDriverWait(driver, 8).until(lambda driver : driver.find_element_by_class_name('section-copy-link-input'))
					currentMapUrl = driver.find_element_by_xpath("//input[@jsaction='pane.copyLink.clickInput']")
					if currentMapUrl:
						currentMapUrl = currentMapUrl.get_attribute("value")
					print(currentMapUrl)
					if soup.find('h1', attrs={'class':'section-hero-header-title-title'}):
						title = soup.find('h1', attrs={'class':'section-hero-header-title-title'}).text
					else:
						title = "FALSE"
					if soup.find('button', attrs={'jsaction':'pane.rating.category'}):
						category = soup.find('button', attrs={'jsaction':'pane.rating.category'}).text
					else:
						category = "FALSE"
					if soup.find('button',attrs={"data-item-id":"address"}):
						address = soup.find('button',attrs={"data-item-id":"address"}).text
						zip = re.match('^.*(?P<zipcode>\d{5}).*$', address).groupdict()['zipcode']
					else:
						address = "FALSE"
						zip = "FALSE"
					if soup.find('div', attrs={'class':'section-open-hours-container'}) and soup.find('span', attrs={'class':'cX2WmPgCkHi__section-info-hour-text'}):
						opening_hours = soup.find('div', attrs={'class':'section-open-hours-container'}).text
						bannedWord = ['Holiday','hours','Hours','might','differ']
						hoursDisplyRegex = re.compile(r"\b(" + "|".join(bannedWord) + ")\\W", re.I)
						hours_displayed = hoursDisplyRegex.sub("",opening_hours).replace(" ", "")
						
						business_status = soup.find('span', attrs={'class':'cX2WmPgCkHi__section-info-hour-text'}).text
						if "closed" in business_status:
							business_status = business_status
						else:
							business_status = "FALSE"
					else:
						hours_displayed = "Closed"
						business_status = "FALSE"
					if ((soup.find('button', attrs={'jsaction':'pane.rating.moreReviews'})) and (soup.find('span', attrs={'class':'section-star-display'}))) is not None:
						reviews = soup.find('button', attrs={'jsaction':'pane.rating.moreReviews'}).text
						reviews = re.sub('[()]', '', reviews)
						reviews_stars = soup.find('span', attrs={'class':'section-star-display'}).text
					else:
						reviews = "FALSE"
						reviews_stars = "FALSE"
					divs = soup.findAll('div', attrs={'class':'gm2-body-2'})
					phone_number="FALSE"
					website = "FALSE"
					claimWebsite = False
					for div in divs:
						phoneNumRegex = re.compile(r'^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$')
						websiteRegex = re.compile(r'^[a-z]+\.com$' , re.M | re.I)
						if phoneNumRegex.search(div.text):
							phone_numbers = div.text
							phone_number = '(%s) %s-%s' % tuple(re.findall(r'\d{4}$|\d{3}', phone_numbers))
						if websiteRegex.search(div.text):
							website = div.text
						if "Claim" in div.text: 
							claimWebsite = True
						
					data.append({
								"import_date": date.today(),
								"phone": phone_number.strip(),
								"business_name": title.strip(),
								"category": category.strip(),
								"address": address.strip(),
								"zip": zip.strip(),
								"website": website.strip(),
								"company": "FALSE",
								"google_maps_link": currentMapUrl,
								"unclaimed": claimWebsite,
								"no_full_address": "FALSE",
								"hours_displayed": hours_displayed.strip(),
								"secondary_number": "",
								"star_count": reviews,
								"amount_of_stars": reviews_stars,
								"business_status": business_status.strip(),
								"business_category": "FALSE"
							})
			driver.close()
			global df
			df = pd.DataFrame(data)
			self.controller.exportCSV('export_csv_files/%s_export_%s.csv' %(filename ,date_time))
		else:
			messagebox.showerror ("Information", "Please select csv file.")
	
	def close_thread(self):
		global thread_running,df
		thread_running = False
		driver.quit()
		df = pd.DataFrame(data)
		self.controller.exportCSV('export_csv_files/%s_export_%s.csv' %(filename ,date_time))

class UploadExcel(Frame):
	def __init__(self,name,controller):
		Frame.__init__(self)
		global v,import_excel_file_path
		self.controller = controller
		w1 = Label(self, text="Upload Excel File:")
		w1.pack()
		w1.place(x=67, y=220)

		self.v = StringVar()
		e1 = Entry(self,width="35", textvariable=self.v)
		e1.pack()
		e1.place(x = 167, y = 220)

		b1 = Button(self,text = "browse" ,command = lambda:self.import_excel_file_data())  
		b1.pack()
		b1.place(x = 407, y = 220)


		b2 = Button(self,text = "upload",command = lambda:self.start_thread())	
		b2.pack()
		b2.place(x = 217, y = 280)
		
		b3 = Button(self,text = "stop",command = lambda:self.close_thread())	
		b3.pack()
		b3.place(x = 277, y = 280)
		
		
	def import_excel_file_data(self):
		self.import_excel_file_path = askopenfilename(filetypes =[('EXCEL Files', '*.xlsx')])
		self.v.set(self.import_excel_file_path)
	
	def start_thread(self):
		self.thread_running = True
		thread = threading.Thread(target=self.upload_excel)
		thread.start()
	
	def upload_excel(self):
	
		if self.import_excel_file_path:
			global filename,date_time,driver,data
			#for get filename
			base=os.path.basename(self.import_excel_file_path)
			filename = os.path.splitext(base)[0]
			#for current Datetime
			now = datetime.now() 
			date_time = now.strftime("%m.%d.%Y_%H%M")
			#read excel file
			readExcelFile = pd.read_excel(self.import_excel_file_path, sheet_name=None)
			
			driver = webdriver.Chrome("chromedriver")
			driver.implicitly_wait(8)
			wait = WebDriverWait(driver, 8)
			data = []
			for key, value in readExcelFile.items(): 
				csvFilePath = 'csv_files/%s_%s' %(filename ,key)
				readExcelFile[key].to_csv(csvFilePath+'.csv', index=False)
				with open(os.path.join(os.getcwd(),''+csvFilePath+'.csv'), mode='r') as csv_file:
					csv_reader = csv.reader(csv_file, delimiter=',')
					
					for row in csv_reader:
						new_row = ''.join(row)
						new_row = new_row.replace('"', '')
						driver.get(new_row)
						element = wait.until_not(
						EC.presence_of_element_located((
						  By.XPATH, "/html/body/div/div/*[name()='svg']")))
						WebDriverWait(driver, 12).until(EC.element_to_be_clickable((By.XPATH, "//button[@jsaction='pane.placeActions.share']"))).click()
						content = driver.page_source
						soup = BeautifulSoup(content,'html.parser')
						currentMapUrl = WebDriverWait(driver, 10).until(
							lambda driver : driver.find_element_by_class_name('section-copy-link-input'))
						if currentMapUrl:
							currentMapUrl = currentMapUrl.get_attribute("value")
						if soup.find('h1', attrs={'class':'section-hero-header-title-title'}):
							title = soup.find('h1', attrs={'class':'section-hero-header-title-title'}).text
						else:
							title = "FALSE"
						if soup.find('button', attrs={'jsaction':'pane.rating.category'}):
							category = soup.find('button', attrs={'jsaction':'pane.rating.category'}).text
						else:
							category = "FALSE"
						if soup.find('button',attrs={"data-item-id":"address"}):
							address = soup.find('button',attrs={"data-item-id":"address"}).text
							zip = re.match('^.*(?P<zipcode>\d{5}).*$', address).groupdict()['zipcode']
						else:
							address = "FALSE"
							zip = "FALSE"
						# currentMapUrl = driver.current_url
						if soup.find('div', attrs={'class':'section-open-hours-container'}) and soup.find('span', attrs={'class':'cX2WmPgCkHi__section-info-hour-text'}):
							opening_hours = soup.find('div', attrs={'class':'section-open-hours-container'}).text
							bannedWord = ['Holiday','hours','Hours','might','differ']
							hoursDisplyRegex = re.compile(r"\b(" + "|".join(bannedWord) + ")\\W", re.I)
							hours_displayed = hoursDisplyRegex.sub("",opening_hours).replace(" ", "")
							
							business_status = soup.find('span', attrs={'class':'cX2WmPgCkHi__section-info-hour-text'}).text
							if "closed" in business_status:
								business_status = business_status
							else:
								business_status = "FALSE"
						else:
							hours_displayed = "Closed"
							business_status = "FALSE"
						if ((soup.find('button', attrs={'jsaction':'pane.rating.moreReviews'})) and (soup.find('span', attrs={'class':'section-star-display'}))) is not None:
							reviews = soup.find('button', attrs={'jsaction':'pane.rating.moreReviews'}).text
							reviews = re.sub('[()]', '', reviews)
							reviews_stars = soup.find('span', attrs={'class':'section-star-display'}).text
						else:
							reviews = "FALSE"
							reviews_stars = "FALSE"
						divs = soup.findAll('div', attrs={'class':'gm2-body-2'})
						phone_number="FALSE"
						website = "FALSE"
						claimWebsite = False
						for div in divs:
							phoneNumRegex = re.compile(r'^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$')
							websiteRegex = re.compile(r'^[a-z]+\.com$' , re.M | re.I)
							if phoneNumRegex.search(div.text):
								phone_numbers = div.text
								phone_number = '(%s) %s-%s' % tuple(re.findall(r'\d{4}$|\d{3}', phone_numbers))
							if websiteRegex.search(div.text):
								website = div.text
							if "Claim" in div.text: 
								claimWebsite = True
						time.sleep(2)
						data.append({
									"import_date": date.today(),
									"phone": phone_number.strip(),
									"business_name": title.strip(),
									"category": category.strip(),
									"address": address.strip(),
									"zip": zip.strip(),
									"website": website.strip(),
									"company": "FALSE",
									"google_maps_link": currentMapUrl.strip(),
									"unclaimed": claimWebsite,
									"no_full_address": "FALSE",
									"hours_displayed": hours_displayed.strip(),
									"secondary_number": "",
									"star_count": reviews,
									"amount_of_stars": reviews_stars,
									"business_status": business_status.strip(),
									"business_category": key
								})
				os.remove(csvFilePath+'.csv')
			self.close_thread()
			# driver.close()
			# global df
			# df = pd.DataFrame(data)
			# self.controller.exportCSV('export_csv_files/%s_export_%s.csv' %(filename ,date_time))
		else:
			messagebox.showerror ("Information", "Please select excel file.")
	
	def close_thread(self):
		global thread_running,df
		thread_running = False
		driver.quit()
		df = pd.DataFrame(data)
		self.controller.exportCSV('export_csv_files/%s_export_%s.csv' %(filename ,date_time))

if __name__ == "__main__":
	app = App()
	app.mainloop()
